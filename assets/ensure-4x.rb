require 'RMagick'
include Magick

# Adjust image sizes to be an even multiple of four
# This is what assetpress expects for android xxxhdpi default sizes.

ARGV.each do |file|
  img = Magick::Image::read(file).first

  w = (img.columns / 4.0).ceil * 4
  h = (img.rows / 4.0).ceil * 4

  if (w != img.columns || h != img.rows)
    puts "#{file} #{img.columns}x#{img.rows} => #{w}x#{h}"

    img = Image.new(w, h) { self.background_color = 'transparent' }
      .composite(img, CenterGravity, OverCompositeOp)
      .write(file)
      # .write("#{File.dirname file}/#{File.basename file, '.*'}_foured.png")
  end
end
