require 'RMagick'
include Magick

# make button states from a single image by creating a negated active state
# renames the original image to foo-default.png and creates foo-active.png
# as invert state.

ARGV.each do |file|
  img = Magick::Image::read(file).first
  dirname = File.dirname file
  basename = File.basename file, '.*'

  # img.write("#{dirname}/#{basename}-default.png")
  File.rename file, "#{dirname}/#{basename}-default.png"
  img.negate.write("#{dirname}/#{basename}-active.png")
end
