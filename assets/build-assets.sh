#!/bin/env bash

cd `dirname $0`


# clean
rm -rf source source\ Resources

# 1. PSD to android

# copy PSD generated assets
mkdir source
cp -r PSD/*-assets/* source
rm source/errors.txt

# round up resolutions
ruby ensure-4x.rb source/*.png

# make invert states for a few buttons
ruby make-states.rb source/nav-*.png

# rename for android (dashes to underscores)
rename -S '-' '_' source/*.*

# run assetpress to generate android pyramids
assetpress -a

# 2. pull over icons

pushd icons
for f in *.zip
do
  d=`basename $f .zip`
  unzip -q $f
  cp -r $d/android/* ../source\ Resources
  rm -r $d
done
popd

# 3. copy to project

cp -r source\ Resources/* ../app/src/main/res/
