package com.pipaslabs.pipas.dummy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class FlightListContent {

    /**
     * An array of sample (dummy) items.
     */
    public static final List<DummyItem> ITEMS = new ArrayList<DummyItem>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DummyItem> ITEM_MAP = new HashMap<String, DummyItem>();

    private static final int COUNT = 7;

    private static final List<String> names = Arrays.asList(new String[] {"azeldis", "neymar"});
    private static final List<String> locations = Arrays.asList(new String[] {"Vidigal", "Oakland", "Brooklyn"});

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDummyItem(i));
        }
    }

    private static void addItem(DummyItem item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static DummyItem createDummyItem(int position) {
        return new DummyItem(
                String.valueOf(position),
                names.get(position % names.size()),
                locations.get(position % locations.size()),
                "+ " + (int)(Math.random()*666),
                12,
                makeComments(position)
        );
    }

    private static List<String> makeComments(int position) {
        ArrayList<String> comments = new ArrayList<>();
        comments.add("<b>neymar</b> I'm gonna take you out");
        return comments;
    }


    /**
     * A dummy item representing a piece of content.
     */
    public static class DummyItem {
        public final String id;
        public final String name;
        public final String location;
        public final String score;
        public final int likes;
        public final List<String> comments;

        public DummyItem(String id, String name, String location, String score, int likes, List<String> comments) {
            this.id = id;
            this.name = name;
            this.location = location;
            this.score = score;
            this.likes = likes;
            this.comments = comments;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
