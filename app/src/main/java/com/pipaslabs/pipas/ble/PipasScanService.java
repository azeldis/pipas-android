package com.pipaslabs.pipas.ble;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.pipaslabs.pipas.viz.DataPoint;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PipasScanService extends Service {
    private static final String TAG = PipasScanService.class.getSimpleName();

    private static final boolean isCannedDemo = false;
    public static final int cannedDeviceFoundDelay = 1008;

    public static final UUID PIPAS_SERVICE_UUID = UUID.fromString("0c3304f4-5f4c-43b3-b7af-50cfba8c8f36");
    private static final UUID[] serviceUUIDs = new UUID[] { PIPAS_SERVICE_UUID };

    public static final String ACTION_DEVICE_FOUND = "com.pipaslabs.pipas.ACTION_DEVICE_FOUND";
    public static final String EXTRA_DEVICE = "com.pipaslabs.pipas.EXTRA_DEVICE";

    private BluetoothManager mBluetoothManager;
    private BluetoothAdapter mBluetoothAdapter;
    private boolean mIsScanning = false;

    private ArrayList<BluetoothDevice> mDevices = new ArrayList<>();
    private PipasDevice pipasDevice;

    public PipasScanService() {
        super();
        Log.d(TAG, "Creating scan service");
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback = new BluetoothAdapter.LeScanCallback() {
        @Override
        public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
            Log.i(TAG, "found device: " + device.getAddress() + " " + device.getName());

            mDevices.add(device);

            Intent i = new Intent();
            i.setAction(ACTION_DEVICE_FOUND);
            i.putExtra(EXTRA_DEVICE, device);
            sendBroadcast(i);

            pipasDevice = new PipasDevice(PipasScanService.this, device);
            pipasDevice.connect();
        }
    };




    public void startScan() {
        if(initialize()) {
            boolean didStart = mBluetoothAdapter.startLeScan(serviceUUIDs, mLeScanCallback);
//            mBluetoothAdapter.startLeScan(mLeScanCallback);
            Log.i(TAG, "starting scan: " + didStart);
            mIsScanning = true;
        }

        if(isCannedDemo) {
            mIsScanning = true;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent i = new Intent();
                    i.setAction(ACTION_DEVICE_FOUND);
//                    i.putExtra(EXTRA_DEVICE, "yay");
                    sendBroadcast(i);
                }
            }, cannedDeviceFoundDelay);

            // a mock device
            pipasDevice = new PipasDevice(PipasScanService.this, null) {
                @Override
                public DataPoint[] getSnapshot() {
                    // return null, processing app will load its own data
                    return null;
                }

                @Override
                public void connect() {
                    // noop
                }
            };
            pipasDevice.connect();
        }

    }

    public void stopScan() {
        mIsScanning = false;
        if(mBluetoothAdapter != null) {
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
    }

    public List<BluetoothDevice> getDevices() {
        return mDevices;
    }

    public PipasDevice getPipasDevice() {
        return pipasDevice;
    }
    
    private boolean initialize() {
        mDevices.clear();
        mIsScanning = false;

        if (mBluetoothManager == null) {
            mBluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            if (mBluetoothManager == null) {
                Log.e(TAG, "Unable to initialize BluetoothManager.");
                return false;
            }
        }

        mBluetoothAdapter = mBluetoothManager.getAdapter();
        if (mBluetoothAdapter == null) {
            Log.e(TAG, "Unable to obtain a BluetoothAdapter.");
            return false;
        }

        return true;
    }

    public boolean isScanning() {
        return mIsScanning;
    }

    public class LocalBinder extends Binder {
        public PipasScanService getService() {
            return PipasScanService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // TODO
        return START_STICKY;
    }

    private IBinder mBinder = new LocalBinder();
}
