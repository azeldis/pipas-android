package com.pipaslabs.pipas.ble;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.util.Log;

import com.pipaslabs.pipas.viz.DataPoint;

import org.apache.commons.math3.geometry.euclidean.threed.CardanEulerSingularityException;
import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.RotationConvention;
import org.apache.commons.math3.geometry.euclidean.threed.RotationOrder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by aez on 7/29/16.
 */
public class PipasDevice {
    private static final String TAG = PipasDevice.class.getSimpleName();
    private final BluetoothDevice device;
    private final Context context;

    private List<DataPoint> data = new ArrayList<DataPoint>();

    private android.bluetooth.BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            List<BluetoothGattService> services = gatt.getServices();

            Log.d(TAG, "services discovered: " + gatt.getServices().size());
            for(BluetoothGattService service : services) {
                Log.d(TAG, "service: " + service.getUuid());
            }

            BluetoothGattService service = gatt.getService(imuServiceUUID);

            List<BluetoothGattCharacteristic> characteristics = service.getCharacteristics();
            Log.d(TAG, "chars: " + characteristics.size());

            BluetoothGattCharacteristic c = characteristics.get(0);
            boolean didStart = gatt.readCharacteristic(c);
            Log.d(TAG, "read didStart: " + didStart);
            boolean didEnable = gatt.setCharacteristicNotification(c, true);
            Log.d(TAG, "notify didEnable: " + didEnable);

            BluetoothGattDescriptor descriptor = c.getDescriptors().get(0);
            Log.d(TAG, "descriptor: " + descriptor);
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
            gatt.writeDescriptor(descriptor);
        }


        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            byte[] bytes = characteristic.getValue();

            int timestamp = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 0);
            handleData(bytes, timestamp);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.d(TAG, "characteristic read: " + characteristic);
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            Log.d(TAG, "characteristic write: " + characteristic);
        }

        @Override
        public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Log.d(TAG, "descriptor read: " + descriptor);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Log.d(TAG, "descriptor write: " + descriptor);
        }

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.d(TAG, "connection state change: " + newState);
            if(newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d(TAG, "connected!");
                boolean didStart = gatt.discoverServices();
                Log.d(TAG, "did start: " + didStart);
            } else if(newState == BluetoothProfile.STATE_DISCONNECTED) {
                Log.d(TAG, "disconnected");
            }
        }
    };

    private void handleData(byte[] bytes, int timestamp) {
        float q0 = SFloat.toFloat(SFloat.getBits(bytes, 4 + 0));
        float q1 = SFloat.toFloat(SFloat.getBits(bytes, 4 + 2));
        float q2 = SFloat.toFloat(SFloat.getBits(bytes, 4 + 4));
        float q3 = SFloat.toFloat(SFloat.getBits(bytes, 4 + 6));
        float accelx = SFloat.toFloat(SFloat.getBits(bytes, 4 + 8));
        float accely = SFloat.toFloat(SFloat.getBits(bytes, 4 + 10));
        float accelz = SFloat.toFloat(SFloat.getBits(bytes, 4 + 12));

        try {
            double[] angles = new Rotation(q0, q1, q2, q3, false).getAngles(RotationOrder.XYZ, RotationConvention.VECTOR_OPERATOR);
            data.add(new DataPoint(timestamp * 0.001f, (float) angles[0], (float) angles[1], (float) angles[2], accelx, accely, accelz));
        } catch (CardanEulerSingularityException e) {
            Log.d(TAG, e.toString());
        }
    }

    private static final UUID imuServiceUUID = UUID.fromString("0c3304f4-5f4c-43b3-b7af-50cfba8c8f36");


    public PipasDevice(Context context, BluetoothDevice device) {
        this.context = context;
        this.device = device;
    }

    public void connect() {
        BluetoothGatt gatt = device.connectGatt(context, false, mGattCallback);
        boolean didConnect = gatt.connect();
        Log.d(TAG, "didConnect: " + didConnect);
    }

    public DataPoint[] getSnapshot() {
        return data.toArray(new DataPoint[data.size()]);
    }
}
