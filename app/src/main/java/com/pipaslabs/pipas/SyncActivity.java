package com.pipaslabs.pipas;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import com.pipaslabs.pipas.ble.PipasScanService;
import com.pipaslabs.pipas.viz.KiteViz;

public class SyncActivity extends AppCompatActivity {
    private static final String TAG = SyncActivity.class.getSimpleName();

    private PipasScanService pipasScanService;
    private ServiceConnection conn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);

        // XXX fake list

        conn =  new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.i(TAG, "started service");
                pipasScanService = ((PipasScanService.LocalBinder) service).getService();
                if (pipasScanService.getPipasDevice() != null) {
                    final KiteViz kiteViz = new KiteViz();
                    kiteViz.setData(pipasScanService.getPipasDevice().getSnapshot());
                    FragmentManager fragmentManager = getFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.container, kiteViz)
                            .commit();

                } else {
                    // we are waiting for device
                    // TODO listen for notification
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                // TODO oh no
            }
        };
        bindService(new Intent(this, PipasScanService.class), conn, BIND_AUTO_CREATE);
    }

    @Override
    protected void onPause() {
        unbindService(conn);
        super.onPause();
    }

    @Override
    public void onAttachFragment(Fragment fragment) {
        super.onAttachFragment(fragment);

        if(fragment instanceof SyncItemFragment) {
            SyncItemFragment f = (SyncItemFragment) fragment;
            switch(f.getId()) {
                case R.id.fragment_0:
                    f.setContent(1, "Ipanema", "12 minutes ago", "+250", true);
                    break;
                case R.id.fragment_1:
                    f.setContent(2, "Rochinha", "3 hours ago", "+325", false);
                    break;
                case R.id.fragment_2:
                    f.setContent(3, "Morro do Vidigal", "yesterday", "+102", false);
                    break;
                case R.id.fragment_3:
                    f.setContent(4, "Rochinha", "3 hours ago", "+325", false);
                    break;
            }
        }
    }

    //
    // actions
    //

    public void close(View sender) {
        finish();
    }
}
