package com.pipaslabs.pipas;


import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.RectEvaluator;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.TimerTask;

/**
 * A simple {@link Fragment} subclass.
 */
public class SyncStatusFragment extends Fragment {


    public static final int DURATION = 7200;
    private View barView;
    private int w;
    private ViewGroup.LayoutParams layoutParams;

    public SyncStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sync_status, container, false);

        barView =  view.findViewById(R.id.sync_status_bar);
        layoutParams = barView.getLayoutParams();

        // we animate in...
        view.setClipBounds(new Rect(0, 0, 0, view.getHeight()));

        return view;
    }

    void start() {
        View view = getView();

        TextView textView = ((TextView) view.findViewById(R.id.sync_status_text));
        textView.setText("Device Detected");

        // progress bar
        int h = view.getHeight();
        int w = view.getWidth();
        Log.d("????", String.format("animating to %d, %d", w, h));
        Rect rFrom = new Rect(0, 0, 0, h);
        Rect rTo = new Rect(0, 0, w, h);
        Animator animator = ObjectAnimator.ofObject(view, "clipBounds", new RectEvaluator(), rFrom, rTo);
        animator.setDuration(DURATION);
        animator.start();
    }

    private void tick() {
        layoutParams.width += 9;
        if(layoutParams.width >= w) {
            layoutParams.width = w;
            done();
        }
        barView.setLayoutParams(layoutParams);
    }

    private void done() {
    }
}
