package com.pipaslabs.pipas.viz;

import processing.core.PVector;

public class DataPoint {

    float sampleTime; // in seconds
    PVector euler;
    PVector linAcc;

    public DataPoint(float sampleTime, float eulerx, float eulery, float eulerz, float accx, float accy, float accz) {
        this.sampleTime = sampleTime;
        euler = new PVector(eulerx, eulery, eulerz);
        linAcc = new PVector(accx, accy, accz);
    }

    public DataPoint(String[] pieces) {
        //multiply by 0.001 to convert from milliseconds to seconds
        sampleTime = toFloat(pieces[0]) * 0.001f;

        //Sensor outputs ZYX, so re-order pieces in the Euler pvector for clarity
        euler = new PVector(toFloat(pieces[3]), toFloat(pieces[2]), toFloat(pieces[1]));
        linAcc = new PVector(toFloat(pieces[4]), toFloat(pieces[5]), toFloat(pieces[6]));
    }

    private float toFloat(String s) {
        return Float.parseFloat(s);
    }
}
