package com.pipaslabs.pipas;

import android.animation.Animator;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;

public class PublishActivity extends AppCompatActivity {

    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_publish);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.publish_toolbar);
        setSupportActionBar(myToolbar);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PUBLISH");
        actionBar.setDisplayHomeAsUpEnabled(true);

        mPager = (ViewPager) findViewById(R.id.pager);
        mPagerAdapter = new PublishFragmentPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.publish_toolbar, menu);
        if(mPager.getCurrentItem() == mPagerAdapter.getCount() - 1) {
            menu.findItem(R.id.action_next).setVisible(false);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                if(mPager.getCurrentItem() == 0) {
                    finish();
                } else {
                    mPager.setCurrentItem(mPager.getCurrentItem() - 1);
                    updateForNewPage();
                }
                break;
            case R.id.action_next:
                mPager.setCurrentItem(mPager.getCurrentItem() + 1);
                updateForNewPage();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateForNewPage() {
        invalidateOptionsMenu();

        // animate the shrinkage
        ViewGroup layout = (ViewGroup) findViewById(R.id.vizImageView);
        layout.setPivotY(1f);
        layout.setPivotX(1f);
        float scaleFactor = 0;
        int duration = 500;
        switch(mPager.getCurrentItem()) {
            case 0:
                scaleFactor = 1;
                break;
            case 1:
                scaleFactor = 0.81f;
                break;
        }

        if(scaleFactor != 0) {
            /*
            final View view = layout;
            final int startHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, view.getHeight(), getResources().getDisplayMetrics());
            final int targetHeight = (int) (startHeight * scaleFactor);
            Animation anim = new Animation() {
                @Override
                protected void applyTransformation(float interpolatedTime, Transformation t) {
                    int newHeight = (int) (startHeight + targetHeight * interpolatedTime);
                    view.getLayoutParams().height = newHeight;
                    view.requestLayout();
                }

                @Override
                public void initialize(int width, int height, int parentWidth, int parentHeight) {
                    super.initialize(width, height, parentWidth, parentHeight);
                }

                @Override
                public boolean willChangeBounds() {
                    return true;
                }
            };
            anim.setDuration(duration);
            layout.startAnimation(anim);
            */

            DisplayMetrics dm = getResources().getDisplayMetrics();
            layout.animate()
                    .scaleX(scaleFactor).scaleY(scaleFactor)
                    .translationX(dm.widthPixels * (1-scaleFactor) * 0.5f )
//                    .translationY(-40 * getResources().getDisplayMetrics().density)
                    .setDuration(duration);
        }
    }

    // actions

    public void publish(View view) {
        // TODO make the parent view show the newly published thing
        finish();
    }
}


class PublishFragmentPagerAdapter extends FragmentPagerAdapter {
    public PublishFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                return new PublishPage0();
            case 1:
                return new PublishPage1();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}

