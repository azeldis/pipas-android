package com.pipaslabs.pipas;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.pipaslabs.pipas.ble.PipasScanService;
import com.pipaslabs.pipas.viz.KiteViz;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    private ServiceConnection conn;
    private PipasScanService pipasScanService;


    // interface-based listening
    private BroadcastReceiver mDeviceFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showViz();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        conn =  new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.i(TAG, "started service");
                pipasScanService = ((PipasScanService.LocalBinder) service).getService();
                if (pipasScanService.getPipasDevice() != null) {
                    showViz();
                } else {
                    // wait for device found broadcast
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                // TODO oh no
            }
        };
        bindService(new Intent(this, PipasScanService.class), conn, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(PipasScanService.ACTION_DEVICE_FOUND);
        registerReceiver(mDeviceFoundReceiver, filter);
    }

    @Override
    protected void onDestroy() {
        unbindService(conn);
        super.onDestroy();
    }

    private void showViz() {
        KiteViz kiteViz = new KiteViz();
        kiteViz.setData(pipasScanService.getPipasDevice().getSnapshot());
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, kiteViz)
                .commit();
    }
}
