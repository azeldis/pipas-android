package com.pipaslabs.pipas;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
//import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * A fragment representing a list of Items.
 */
public class SyncItemFragment extends Fragment {

    private int index;
    private String placeName;
    private String whenString;
    private String scoreString;
    private boolean isOpen;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public SyncItemFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_syncitem, container, false);

        ((TextView)view.findViewById(R.id.id)).setText(String.valueOf(index));
        ((TextView)view.findViewById(R.id.whereText)).setText(placeName);
        ((TextView)view.findViewById(R.id.whenText)).setText(whenString);
        ((TextView)view.findViewById(R.id.scoreText)).setText(scoreString);

        int disclosureDrawable = isOpen ? R.drawable.ic_keyboard_arrow_up_black_36dp : R.drawable.ic_keyboard_arrow_down_black_36dp;
        int tintColor = isOpen ? Color.BLACK : Color.LTGRAY;
        Drawable drawable = DrawableCompat.wrap(ContextCompat.getDrawable(inflater.getContext(), disclosureDrawable));
        DrawableCompat.setTint(drawable, tintColor);
        ImageView imageView = (ImageView) view.findViewById(R.id.listDisclosureIcon);
        imageView.setImageDrawable(drawable);

        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    // set the content for this fragment (we aren't using arguments, because XML);
    // NOTE this must be called before onCreateView()
    public void setContent(int index, String placeName, String whenString, String scoreString, boolean isOpen) {
        this.index = index;
        this.placeName = placeName;
        this.whenString = whenString;
        this.scoreString = scoreString;
        this.isOpen = isOpen;
    }
}
