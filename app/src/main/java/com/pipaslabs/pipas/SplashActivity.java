package com.pipaslabs.pipas;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

import com.ncapdevi.fragnav.FragNavController;
import com.pipaslabs.pipas.ble.PipasScanService;
import com.pipaslabs.pipas.dummy.FlightListContent;

import java.util.ArrayList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

// in the future we may want to hew closer to material design, and use https://github.com/roughike/BottomBar

public class SplashActivity extends AppCompatActivity
        implements FlightFragment.OnListFragmentInteractionListener, BattlesFragment.OnFragmentInteractionListener, MeFragment.OnFragmentInteractionListener {
    private static final String TAG = SplashActivity.class.getSimpleName();

    private FragNavController mFragNavController;
    private View mPreviousTab;

    private ServiceConnection serviceConnection;

    // interface-based listening
    private BroadcastReceiver mDeviceFoundReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showDeviceFoundOverlay();
        }
    };

    // dynamic listening
    private void showDeviceFoundOverlay() {
        View syncStatusView = findViewById(R.id.sync_status_fragment);
        syncStatusView.setAlpha(0f);
        syncStatusView.setVisibility(View.VISIBLE);
        syncStatusView.animate()
                .alpha(1f)
                .setDuration(810)
                .setListener(null);

        final SyncStatusFragment frag = (SyncStatusFragment) getSupportFragmentManager().findFragmentById(R.id.sync_status_fragment);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                frag.start();
            }
        });
    }

    private PipasScanService pipasScanService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        requestWindowFeature(Window.FEATURE_ACTION_BAR);
//        getSupportActionBar().hide();

        setContentView(R.layout.activity_splash);

        List<Fragment> fragments = new ArrayList<>(5);
        fragments.add(FlightFragment.newInstance());
        fragments.add(BattlesFragment.newInstance());
        fragments.add(NowFragment.newInstance());
        fragments.add(MeFragment.newInstance());
        mFragNavController = new FragNavController(getSupportFragmentManager(), R.id.container, fragments);

        findViewById(R.id.sync_status_fragment).setVisibility(View.GONE);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter();
        filter.addAction(PipasScanService.ACTION_DEVICE_FOUND);
        registerReceiver(mDeviceFoundReceiver, filter);

//        startService(new Intent(this, PipasScanService.class));
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.i(TAG, "started service");
                pipasScanService = ((PipasScanService.LocalBinder) service).getService();
                if (pipasScanService.getPipasDevice() != null) {
                    showDeviceFoundOverlay();
                } else if (!pipasScanService.isScanning()) {
                    pipasScanService.startScan();
                } else {
                    // wait for device found broadcast
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        bindService(new Intent(this, PipasScanService.class), serviceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        unbindService(serviceConnection);
        super.onDestroy();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    //
    // actions
    //

    public void showSyncStatus(View sender) {
        startActivity(new Intent(this, SyncActivity.class));
        startActivity(new Intent(this, MainActivity.class));
    }

    public void publish(View tab) {
//        startActivity(new Intent(this, MainActivity.class));
        startActivity(new Intent(this, PublishActivity.class));
    }

    public void selectTab(View tab) {
        if(tab == mPreviousTab) {
            // TODO scroll to top
            return;
        }
        ViewGroup parent = (ViewGroup) tab.getParent();
        int index = parent.indexOfChild(tab);

        //noinspection WrongConstant
        mFragNavController.switchTab(index);

        tab.setSelected(true);
        if(mPreviousTab != null) {
            mPreviousTab.setSelected(false);
        }
        mPreviousTab = tab;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        // TODO handle fragment interaction
    }

    @Override
    public void onListFragmentInteraction(FlightListContent.DummyItem item) {
        // TODO handle list interaction
    }
}
